//--------------------------------------------------------------
// File     : main.c
// Datum    : 27.07.2015
// Version  : 1.0
// Autor    : UB
// EMail    : mc-4u(@)t-online.de
// Web      : www.mikrocontroller-4u.de
// CPU      : STM32F746
// Board    : STM32F746-Discovery-Board
// IDE      : OpenSTM32
// GCC      : 4.9 2015q2
// Module   : CubeHAL
// Funktion : Hauptprogramm
//--------------------------------------------------------------


#include "stm32_ub_lcd_480x272.h"
#include "stm32_ub_font.h"

void InitLCD(void)
{
  UB_LCD_Init();
  UB_LCD_LayerInit_Fullscreen();
  // auf Hintergrund schalten
  UB_LCD_SetLayer_1();
  // Hintergrund komplett mit einer Farbe f�llen
  UB_LCD_FillLayer(0);
  // auf Vordergrund schalten
  UB_LCD_SetLayer_2();
  // Vordergrund komplett mit einer Farbe f�llen
  UB_LCD_FillLayer(0);
}

void main_app();

int main(void)
{
  UB_System_Init();

  InitLCD();

  main_app();

  while(1)
  {
  }
}
