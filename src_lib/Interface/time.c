#include <time.h>

#include <stm32f7xx_hal.h>

void delay_ms(size_t ms) {
    HAL_Delay(ms);
}

size_t get_tick() {
    return HAL_GetTick();
}
