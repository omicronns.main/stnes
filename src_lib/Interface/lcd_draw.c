#include <lcd_draw.h>

#include <stm32_ub_lcd_480x272.h>
#include <stm32_ub_sdram.h>

#include <string.h>

static volatile uint16_t *frame1 = (uint16_t *)LCD_FRAME_BUFFER;
static volatile uint16_t *frame2 = (uint16_t *)LCD_FRAME_BUFFER + LCD_MAX_PIXELS;

static void draw_frame(volatile uint16_t *frame, lcd_pos_t *pos, lcd_rect_t const *rect, uint16_t color) {
    *(frame + pos->y * LCD_MAXW + pos->x) = color;

    pos->x += 1;

    if (pos->x >= rect->pos.x + rect->w) {
        pos->y += 1;
        pos->x = rect->pos.x;
    }
    if (pos->y >= rect->pos.y + rect->h) {
        pos->y = rect->pos.y;
    }
}

void lcd_fill_frame1(uint16_t color) {
    for (int pixel = 0; pixel < LCD_MAX_PIXELS; ++pixel)
        *(frame1 + pixel) = color;
}

void lcd_fill_frame2(uint16_t color) {
    for (int pixel = 0; pixel < LCD_MAX_PIXELS; ++pixel)
        *(frame2 + pixel) = color;
}

void lcd_draw_frame1(lcd_pos_t *pos, lcd_rect_t const *rect, uint16_t color) {
    draw_frame(frame1, pos, rect, color);
}

void lcd_draw_frame2(lcd_pos_t *pos, lcd_rect_t const *rect, uint16_t color) {
    draw_frame(frame2, pos, rect, color);
}

void lcd_show_frame1() {
    UB_LCD_SetLayer_1();
    UB_LCD_Refresh();
}

void lcd_show_frame2() {
    UB_LCD_SetLayer_2();
    UB_LCD_Refresh();
}
