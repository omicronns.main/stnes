#include <lcd_draw.h>
#include <time.h>

#include <nes_system.h>

uint16_t rgb565(uint8_t r, uint8_t g, uint8_t b) {
    return (((r >> 3) & 0x1f) << 11) | (((g >> 2) & 0x3f) << 5) | ((b >> 3) & 0x1f);
}

extern uint8_t rom_data[];
extern std::size_t rom_len;

extern "C" void main_app() {
    uint16_t palette[] =
    {
        rgb565(84,  84,  84),   rgb565(0,  30, 116),    rgb565(8,  16, 144),    rgb565(48,   0, 136),
        rgb565(68,   0, 100),   rgb565(92,   0,  48),   rgb565(84,   4,   0),   rgb565(60,  24,   0),
        rgb565(32,  42,   0),   rgb565(8,  58,   0),    rgb565(0,  64,   0),    rgb565(0,  60,   0),
        rgb565(0,  50,  60),    rgb565(0,   0,   0),    rgb565(0, 0, 0),        rgb565(0, 0, 0),
        rgb565(152, 150, 152),  rgb565(8,  76, 196),    rgb565(48,  50, 236),   rgb565(92,  30, 228),
        rgb565(136,  20, 176),  rgb565(160,  20, 100),  rgb565(152,  34,  32),  rgb565(120,  60,   0),
        rgb565(84,  90,   0),   rgb565(40, 114,   0),   rgb565(8, 124,   0),    rgb565(0, 118,  40),
        rgb565(0, 102, 120),    rgb565(0,   0,   0),    rgb565(0, 0, 0),        rgb565(0, 0, 0),
        rgb565(236, 238, 236),  rgb565(76, 154, 236),   rgb565(120, 124, 236),  rgb565(176,  98, 236),
        rgb565(228,  84, 236),  rgb565(236,  88, 180),  rgb565(236, 106, 100),  rgb565(212, 136,  32),
        rgb565(160, 170,   0),  rgb565(116, 196,   0),  rgb565(76, 208,  32),   rgb565(56, 204, 108),
        rgb565(56, 180, 204),   rgb565(60,  60,  60),   rgb565(0, 0, 0),        rgb565(0, 0, 0),
        rgb565(236, 238, 236),  rgb565(168, 204, 236),  rgb565(188, 188, 236),  rgb565(212, 178, 236),
        rgb565(236, 174, 236),  rgb565(236, 174, 212),  rgb565(236, 180, 176),  rgb565(228, 196, 144),
        rgb565(204, 210, 120),  rgb565(180, 222, 120),  rgb565(168, 226, 144),  rgb565(152, 226, 180),
        rgb565(160, 214, 228),  rgb565(160, 162, 160),  rgb565(0, 0, 0),        rgb565(0, 0, 0)
    };

    nes_system system;
    system.power_on();
    system.load_rom(rom_data, rom_len, nes_rom_exec_mode_reset);

    auto prev_tick = get_tick();

    //
    // Game main loop
    //
    bool frame = false;
    while (true)
    {
        auto curr_tick = get_tick();

        auto delta_ticks = curr_tick - prev_tick;

        if (delta_ticks == 0)
            delta_ticks = 1;

        prev_tick = curr_tick;

        auto cpu_cycles = (NES_CLOCK_HZ * delta_ticks) / 1000;

        // Avoids a scenario where the loop keeps getting longer
        if (cpu_cycles > nes_cycle_t(NES_CLOCK_HZ))
            cpu_cycles = nes_cycle_t(NES_CLOCK_HZ);

        for (nes_cycle_t i = nes_cycle_t(0); i < cpu_cycles; ++i)
            system.step(nes_cycle_t(1));

        uint8_t *frame_buffer = system.ppu()->frame_buffer();
        if (frame) {
            lcd_pos_t pos {};
            lcd_rect_t rect { pos, PPU_SCREEN_X, PPU_SCREEN_Y };
            auto pixels = rect.h * rect.w;
            for (auto i = 0; i < pixels; ++i) {
                    lcd_draw_frame1(&pos, &rect, palette[(*frame_buffer & 0xff)]);
                    frame_buffer++;
            }
            lcd_show_frame1();
        }
        else {
            lcd_pos_t pos {};
            lcd_rect_t rect { pos, PPU_SCREEN_X, PPU_SCREEN_Y };
            auto pixels = rect.h * rect.w;
            for (auto i = 0; i < pixels; ++i) {
                    lcd_draw_frame2(&pos, &rect, palette[(*frame_buffer & 0xff)]);
                    frame_buffer++;
            }
            lcd_show_frame2();
        }

        frame = !frame;
    }
}
