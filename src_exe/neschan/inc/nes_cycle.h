#pragma once

#include <cstdint>

typedef std::size_t nes_cycle_t;
typedef std::size_t nes_ppu_cycle_t;
typedef std::size_t nes_cpu_cycle_t;

#define PPU_SCANLINE_CYCLE nes_ppu_cycle_t(341)
#define NES_CLOCK_HZ (21477272ll / 4)
