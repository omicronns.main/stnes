import re
import conans
import conans.errors
import conans.tools


class ConanFileInst(conans.ConanFile):
    name = "stnes"
    version = "0.1.0"
    url = "https://gitlab.com/omicronns.main/stnes.git"
    author = "omicronns@gmail.com"
    license = "MIT"
    description = "missing"
    generators = "txt", "cmake"
    settings = "os", "arch", "compiler", "build_type"
    exports_sources = "*", "!.conan", "!.build"


    def build_requirements(self):
        if self.develop and not conans.tools.cross_building(self.settings):
            self.build_requires("gtest/1.8.0@bincrafters/stable")

    def header_only(self):
        return False

    def compile_executables(self):
        return True

    def compile_tests(self):
        return self.develop and not conans.tools.cross_building(self.settings)

    def profile_cortexm(self):
        return re.match(r"^cortex-m(0|0plus|3|4|7)$", str(self.settings.arch)) is not None

    def cmake_defs(self):
        defs = {}
        defs["HEADER_ONLY"] = "1" if self.header_only() else "0"
        defs["CMAKE_SYSTEM_PROCESSOR"] = str(self.settings.arch)
        defs["CONAN_PROJECT_NAME"] = str(self.name)
        if self.compile_executables():
            defs["COMPILE_EXECUTABLES"] = "1"
        if self.compile_tests():
            defs["COMPILE_TESTS"] = "1"
        if self.profile_cortexm():
            defs["CMAKE_TOOLCHAIN_FILE"] = "cmake/toolchain/arm-none-eabi-gcc.cmake"
            if "ARM_FLAGS_FPU" in self.env:
                defs["ARM_FLAGS_FPU"] = self.env["ARM_FLAGS_FPU"]
            if "ARM_FLAGS_LINKER_SPECS" in self.env:
                defs["ARM_FLAGS_LINKER_SPECS"] = self.env["ARM_FLAGS_LINKER_SPECS"]
        return defs

    def configure(self):
        if self.profile_cortexm():
            if str(self.settings.os) not in ["Generic"]:
                conans.errors.ConanException("settings.os must be \"Generic\"")
            if str(self.settings.compiler) not in ["gcc"]:
                conans.errors.ConanException("settings.compiler must be \"gcc\"")
            self.settings.compiler["gcc"].remove(("libcxx", "threads", "exception"))

    def build(self):
        if not self.header_only() or self.compile_tests():
            cmake = conans.CMake(self)
            cmake.configure(defs=self.cmake_defs())
            if not self.develop or ("build" in self.env and self.env["build"]):
                cmake.build()

    def imports(self):
        self.copy("*", dst="import", src="export")

    def package(self):
        self.copy("*", dst="export", src="export")
        self.copy("*", dst="include", src="include")
        for ext in ["a", "lib", "pdb"]:
            self.copy("*.{}".format(ext), dst="lib", src="lib")
        for ext in ["dll", "exe", "out", "elf", "hex", "bin"]:
            self.copy("*.{}".format(ext), dst="bin", src="bin")

    def package_info(self):
        self.cpp_info.libs = [str(self.name)]

    def package_id(self):
        if self.header_only():
            self.info.header_only()
