#pragma once

#include <stdint.h>
#include <stddef.h>

#define  LCD_MAXW (480)
#define  LCD_MAXH (272)
#define  LCD_MAX_PIXELS (LCD_MAXH * LCD_MAXW)

typedef struct {
    size_t x;
    size_t y;
} lcd_pos_t;

typedef struct {
    lcd_pos_t pos;
    size_t w;
    size_t h;
} lcd_rect_t;


#ifdef __cplusplus
extern "C" {
#endif

void lcd_fill_frame1(uint16_t color);
void lcd_fill_frame2(uint16_t color);

void lcd_draw_frame1(lcd_pos_t *pos, lcd_rect_t const *rect, uint16_t color);
void lcd_draw_frame2(lcd_pos_t *pos, lcd_rect_t const *rect, uint16_t color);

void lcd_show_frame1();
void lcd_show_frame2();

#ifdef __cplusplus
}
#endif
