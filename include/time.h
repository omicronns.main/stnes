#pragma once

#include <stdint.h>
#include <stddef.h>


#ifdef __cplusplus
extern "C" {
#endif

void delay_ms(size_t ms);
size_t get_tick();

#ifdef __cplusplus
}
#endif
